package com.example.quangale.geoquiz;

public class TrueFalse {
    private int mQuestion;

    private boolean  mTrueQuestion;

    public TrueFalse(int question,boolean trueQuestion){
        mTrueQuestion = trueQuestion;
        mQuestion = question;

    }
    public int getQuestion() {
        return mQuestion;
    }

    public void setQuestion(int mQuestion) {
        this.mQuestion = mQuestion;
    }

    public boolean isTrueQuestion() {
        return mTrueQuestion;
    }

    public void setTrueQuestion(boolean mTrueQuestion) {
        this.mTrueQuestion = mTrueQuestion;
    }
}
